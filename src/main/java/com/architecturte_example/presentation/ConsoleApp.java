package com.architecturte_example.presentation;

import com.architecturte_example.service.Book;
import com.architecturte_example.service.IBookService;

import java.sql.Date;
import java.util.Scanner;

public class ConsoleApp {
    private final Scanner scanner;
    private final IBookService bookService;

    public ConsoleApp(Scanner scanner, IBookService bookService) {
        this.scanner = scanner;
        this.bookService = bookService;
    }

    public void start(){
        System.out.println("Hello!");

        var option = chooseOption();
        perform(option);
    }

    private String chooseOption(){
        System.out.println("What do you want to do?");
        System.out.println("(1) Add book");
        System.out.println("(2) Show all");
        System.out.println("(3) Show one by id");

        return scanner.nextLine();
    }

    private void perform(String option) {
        switch (option){
            case "1":
            case "Add book":
                addBook();
                break;
            case "2":
            case "Show all":
                showAllBooks();
                break;
            case "3":
            case "Show one by id":
                showOneBook();
                break;
        }
    }

    private void addBook(){
        System.out.println("Provide title");
        var title = scanner.nextLine();

        System.out.println("Provide author");
        var author = scanner.nextLine();

        System.out.println("Provide isbn");
        var isbn = scanner.nextLine();

        System.out.println("Provide publication year");
        var date = scanner.nextLine();

        bookService.addBook(new Book(title, author, isbn, Date.valueOf(date)));
    }

    private void showAllBooks(){
        var books = bookService.getAllBooks();

        for(var book : books){
            System.out.println(book);
        }
    }

    private void showOneBook(){
        System.out.println("Provide book id: ");
        var id = scanner.nextInt();
        scanner.nextLine();
        var book = bookService.getOneBookById(id);
        System.out.println(book);
    }
}
