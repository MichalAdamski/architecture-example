package com.architecturte_example.service;

import java.util.List;

public interface IBookService {
    void addBook(Book book);
    List<Book> getAllBooks();
    Book getOneBookById(int id);
}
