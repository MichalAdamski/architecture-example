package com.architecturte_example.service;

import com.architecturte_example.persistance.IBookRepository;
import com.architecturte_example.persistance.entity.BooksEntity;

import java.util.List;
import java.util.stream.Collectors;

public class BookService implements IBookService {
    private final IBookRepository bookRepository;

    public BookService(IBookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public void addBook(Book book) {
        bookRepository.addBook(mapBookToBooksEntity(book));
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.getAllBooks().stream().map(this::mapBooksEntityToBook).collect(Collectors.toList());
    }

    @Override
    public Book getOneBookById(int id) {
        return mapBooksEntityToBook(bookRepository.getOneById(id));
    }

    private Book mapBooksEntityToBook(BooksEntity booksEntity){
        if (booksEntity == null){
            return null;
        }
        return new Book(booksEntity.getId(), booksEntity.getTitle(), booksEntity.getAuthor(), booksEntity.getIsbn(), booksEntity.getPublishedDate());
    }

    private BooksEntity mapBookToBooksEntity(Book book){
        var bookEntity = new BooksEntity();
        bookEntity.setId(book.getId());
        bookEntity.setAuthor(book.getAuthor());
        bookEntity.setTitle(book.getTitle());
        bookEntity.setIsbn(book.getIsbn());
        bookEntity.setPublishedDate(book.getPublishedDate());

        return bookEntity;
    }
}
