package com.architecturte_example;

import com.architecturte_example.persistance.BookRepository;
import com.architecturte_example.presentation.ConsoleApp;
import com.architecturte_example.service.BookService;
import jakarta.persistence.Persistence;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        // setup
        var scanner = new Scanner(System.in);
        var emf = Persistence.createEntityManagerFactory("default");
        var em = emf.createEntityManager();

        var bookRepository = new BookRepository(em);
        var bookService = new BookService(bookRepository);
        var consoleApp = new ConsoleApp(scanner, bookService);

        // start
        consoleApp.start();

        // close app
        scanner.close();
        em.close();
        emf.close();
    }
}
