package com.architecturte_example.persistance;

import com.architecturte_example.persistance.entity.BooksEntity;
import jakarta.persistence.EntityManager;

import java.util.List;

public class BookRepository implements IBookRepository {
    private final EntityManager entityManager;

    public BookRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void addBook(BooksEntity booksEntity) {
        var transaction = entityManager.getTransaction();
        transaction.begin();

        entityManager.persist(booksEntity);

        transaction.commit();
    }

    @Override
    public List<BooksEntity> getAllBooks() {
        var stringQuery = "SELECT b FROM BooksEntity b";
        var query = entityManager.createQuery(stringQuery, BooksEntity.class);

        return query.getResultList();
    }

    @Override
    public BooksEntity getOneById(int id) {
        return entityManager.find(BooksEntity.class, id);
    }
}
