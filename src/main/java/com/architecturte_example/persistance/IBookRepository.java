package com.architecturte_example.persistance;

import com.architecturte_example.persistance.entity.BooksEntity;

import java.util.List;

public interface IBookRepository {
    void addBook(BooksEntity booksEntity);
    List<BooksEntity> getAllBooks();
    BooksEntity getOneById(int id);
}
