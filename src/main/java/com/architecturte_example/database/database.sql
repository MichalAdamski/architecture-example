DROP DATABASE IF EXISTS book_management;
CREATE DATABASE book_management;

USE book_management;

CREATE TABLE books (
                       id BIGINT AUTO_INCREMENT PRIMARY KEY,
                       title VARCHAR(255) NOT NULL,
                       author VARCHAR(255) NOT NULL,
                       isbn VARCHAR(255) NOT NULL,
                       published_date DATE
);

INSERT INTO books (title, author, isbn, published_date) VALUES
                                                            ('1984', 'George Orwell', '978-0451524935', '1949-06-08'),
                                                            ('To Kill a Mockingbird', 'Harper Lee', '978-0446310789', '1960-07-11'),
                                                            ('The Great Gatsby', 'F. Scott Fitzgerald', '978-0743273565', '1925-04-10'),
                                                            ('One Hundred Years of Solitude', 'Gabriel García Márquez', '978-0060883287', '1967-06-05'),
                                                            ('Brave New World', 'Aldous Huxley', '978-0060850524', '1932-01-01');

SELECT * FROM books;
